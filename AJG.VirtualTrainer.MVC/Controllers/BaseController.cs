﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using VirtualTrainer.Interfaces;
using VirtualTrainer;
using VirtualTrainer.DataAccess;
using AJG.VirtualTrainer.Services;

namespace AJG.VirtualTrainer.MVC.Controllers
{
    public class BaseController : Controller
    {
        public IUnitOfWork Uow;
        public AdminService AdminService;

        public BaseController()
        {
            Uow = new UnitOfWork();
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            AdminService = new AdminService(Uow);
        }
        public void Dispose()
        {
            Uow.Dispose();
            AdminService.Dispose();
        }
    }
}