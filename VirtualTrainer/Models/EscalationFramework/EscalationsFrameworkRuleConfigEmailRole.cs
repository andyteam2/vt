﻿using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Configuration;
using System.Net.Configuration;
using System.IO;

namespace VirtualTrainer
{
    public class EscalationsFrameworkRuleConfigEmailRole : EscalationsFrameworkRuleConfigEmail
    {
        #region [ EF properties ]

        [ForeignKey("Action")]
        [Required]
        public int ActionId { get; set; }
        public EscalationsFrameworkAction Action { get; set; }
        public string OverrideRecipientEmail { get; set; }

        #endregion

        #region [ Non EF properties ]

        [NotMapped]
        public string ActionName { get; set; }

        #endregion

        public override void RunEscalationConfiguration(VirtualTrainerContext ctx, bool updateTimeStamp, string ServerRazorEmailTemplatePath)
        {
            EmailRuleConfigEscalationsActionTakenLog log = new EmailRuleConfigEscalationsActionTakenLog(this, ctx);

            try
            {
                LoadRequiredContextObjects(ctx);
                if (this.Schedule.IsScheduledToRun(ctx, this.LastRunDate) || updateTimeStamp == false)
                {
                    if (this.IsActive)
                    {
                        List<BreachLog> breachLogs = GetUniqueBreachLogsFromBreachSources(ctx);

                        if (breachLogs.Any())
                        {
                            if (this.GetUniqueUsersFromBreachSources(ctx).Any())
                            {
                                using (SmtpClient SmtpClient = new SmtpClient())
                                {
                                    foreach (User user in this.GetUniqueUsersFromBreachSources(ctx))
                                    {
                                        EmailUser(user, ctx, breachLogs, SmtpClient, ServerRazorEmailTemplatePath);
                                    }
                                }
                                log.ExecutionOutcome = EscalationsActionTakenLogOutcome.ExecutionSuccess;
                            }
                            else
                            {
                                log.ExecutionOutcome = EscalationsActionTakenLogOutcome.EscalationsRuleConfigNoRecipients;
                            }
                        }
                        else
                        {
                            log.ExecutionOutcome = EscalationsActionTakenLogOutcome.EscalationsRuleConfigNoBreachLogs;
                        }
                    }
                    else
                    {
                        log.ExecutionOutcome = EscalationsActionTakenLogOutcome.EscalationsRuleConfigNotActive;
                    }
                }
                else
                {
                    log.ExecutionOutcome = EscalationsActionTakenLogOutcome.EscalationsRuleConfigNotScheduledToRun;
                }
                if (updateTimeStamp)
                {
                    this.LastRunDate = DateTime.Now;
                }
                log.Success = true;
            }
            catch (Exception ex)
            {
                // Log Exception
                SystemLog errorLog = new SystemLog(ex, this.Project);
                ctx.SystemLogs.Add(errorLog);
                // update Rule
                log.ErrorLogEntry = errorLog;
                log.ErrorMessage = ex.Message;
                log.Success = false;
                log.ExecutionOutcome = EscalationsActionTakenLogOutcome.Failure;
            }
            finally
            {
                log.TimeStamp = DateTime.Now;
                log.Finish = DateTime.Now;
                this.ActionTakenLog.Add(log);
                ctx.SaveChanges();
            }
        }

        private void EmailUser(User user, VirtualTrainerContext ctx, List<BreachLog> breaches, SmtpClient SmtpClient, string ServerRazorEmailTemplatePath)
        {
            EmailRuleConfigEscalationsActionTakenLog log = new EmailRuleConfigEscalationsActionTakenLog(this, ctx);
            try
            {
                //  only process if user is active and they have the role required by this ActionId.
                if (user.IsActive)
                {
                    if (user.HasRole(ctx, Action.RoleId))
                    {
                        List<BreachLog> breachesForUser = FilterBreaches(ctx, breaches, user);
                        
                        if (breachesForUser.Any())
                        {
                            // Create the Razor Object model passing in only stuff relevant to the user.
                            EscalationsFrameworkConfigEmailRazorModel razorModel = new EscalationsFrameworkConfigEmailRazorModel(ctx)
                            {
                                BreachLogs = breachesForUser,
                                Recipient = user,
                                EFEmailRuleConfig = this,
                                SendEmail = true,
                                SentFromName = this.InternalSentFromUserName,
                                AttachExcelofBreaches = this.AttachExcelOfBreaches
                            };

                            string emailBodyTemplateName = this.EmailBodyTemplate;
                            string emailSubjectTemplateName = string.Format("emailSubjectTemplateKey{0}{1}", this.ProjectId, this.Id);
                            string emailHtmlBody = string.Empty;
                            string emailHtmlSubject = string.Empty;

                            //Razor.Parse(this.EmailBodyTemplate, razorModel);

                            if (Engine.Razor.IsTemplateCached(emailBodyTemplateName, typeof(EscalationsFrameworkConfigEmailRazorModel)))
                            {
                                emailHtmlBody = Engine.Razor.Run(emailBodyTemplateName, typeof(EscalationsFrameworkConfigEmailRazorModel), razorModel, null);
                            }
                            else
                            {
                                string filePath = string.Format("{0}\\{1}", ServerRazorEmailTemplatePath, this.EmailBodyTemplate);
                                if (System.IO.File.Exists(filePath))
                                {
                                    string emailRazorTemplate = System.IO.File.OpenText(filePath).ReadToEnd();
                                    emailHtmlBody = Engine.Razor.RunCompile(emailRazorTemplate, emailBodyTemplateName, typeof(EscalationsFrameworkConfigEmailRazorModel), razorModel, null);
                                }
                            }

                            if (Engine.Razor.IsTemplateCached(emailSubjectTemplateName, typeof(EscalationsFrameworkConfigEmailRazorModel)))
                            {
                                emailHtmlSubject = Engine.Razor.Run(emailSubjectTemplateName, typeof(EscalationsFrameworkConfigEmailRazorModel), razorModel, null);
                            }
                            else
                            {
                                emailHtmlSubject = Engine.Razor.RunCompile(this.EmailSubjectTemplate, emailSubjectTemplateName, typeof(EscalationsFrameworkConfigEmailRazorModel), razorModel, null);
                            }
                            
                            // For some reason templateservice parse seems to html endcode brackets resulting in <div> etc being rendered!!
                            emailHtmlBody = emailHtmlBody.Replace("&lt;", "<").Replace("&gt;", ">");
                            emailHtmlSubject = emailHtmlSubject.Replace("&lt;", "<").Replace("&gt;", ">");

                            // Populate log with email details.
                            log.PopulatEmailAndUserDetails(ctx, user, emailHtmlBody, emailHtmlSubject, this.InternalSentFromEmail);

                            string toEmail = string.IsNullOrEmpty(this.OverrideRecipientEmail) ? user.Email : this.OverrideRecipientEmail;
                            log.EmailTo = toEmail;

                            if (razorModel.SendEmail)
                            {
                                if (!string.IsNullOrEmpty(toEmail))
                                {
                                    // Send the email
                                    SendEmail(emailHtmlBody, toEmail, emailHtmlSubject, razorModel, ctx, SmtpClient);
                                    log.ExecutionOutcome = EscalationsActionTakenLogOutcome.ExecutionSuccess;
                                }
                                else
                                {
                                    log.ExecutionOutcome = EscalationsActionTakenLogOutcome.EscalationsRuleConfigEmailUserHasNoEmail;
                                }
                            }
                            else
                            {
                                log.ExecutionOutcome = EscalationsActionTakenLogOutcome.EscalationsRuleConfigEmailRazorModelSetSendEmailToFalse;
                            }
                        }
                        else
                        {
                            log.ExecutionOutcome = EscalationsActionTakenLogOutcome.EscalationsRuleConfigEmailNoBreacheLogsForUser;
                        }
                    }
                }
                else
                {
                    log.ExecutionOutcome = EscalationsActionTakenLogOutcome.EscalationsRuleConfigEmailUserNotActive;
                }
                log.Success = true;
            }
            catch (Exception ex)
            {
                // Log Exception
                SystemLog errorLog = new SystemLog(ex, this.Project);
                ctx.SystemLogs.Add(errorLog);
                // update Rule
                log.ErrorLogEntry = errorLog;
                log.ErrorMessage = ex.Message;
                log.Success = false;
                log.ExecutionOutcome = EscalationsActionTakenLogOutcome.Failure;
            }
            finally
            {
                log.TimeStamp = DateTime.Now;
                log.Finish = DateTime.Now;
                this.ActionTakenLog.Add(log);
                ctx.SaveChanges();
            }
        }

        private List<BreachLog> FilterBreaches(VirtualTrainerContext ctx, List<BreachLog> breaches, User user)
        {
            switch (this.ActionId)
            {
                case ((int)ActionEnum.EmailToHandler):
                    return breaches.Where(u => u.UserId == user.Id).ToList();
                case ((int)ActionEnum.EmailToTeamLead):
                    return breaches.Where(u => u.IsUserATeamLeadForBreach(ctx, user)).ToList();
                case ((int)ActionEnum.EmailToBranchManager):
                    return breaches.Where(u => u.IsUserAManagerForThisBreach(ctx, user)).ToList();
                case ((int)ActionEnum.EmailToQualityAuditor):
                    return breaches.Where(u => u.IsUserAQualityAuditorForBreach(ctx, user)).ToList();
                case ((int)ActionEnum.EmailToRegionalManager):
                    return breaches.Where(u => u.IsUserARegionalManagerForBreach(ctx, user)).ToList();
            }

            return breaches;
        }

        private new void LoadRequiredContextObjects(VirtualTrainerContext ctx)
        {
            if (!ctx.Entry(this).Reference("Action").IsLoaded)
            {
                ctx.Entry(this).Reference("Action").Load();
            }

            base.LoadRequiredContextObjects(ctx);
        }
    }
}