﻿using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using VirtualTrainer.Utilities;

namespace VirtualTrainer
{
    public abstract class EscalationsFrameworkRuleConfigEmail : EscalationsFrameworkRuleConfig
    {
        #region [EF Mapped Properties]

        //public EscalationsFrameworkEmailCollationOption EmailCollationOption { get; set; }
        public string SentFromEmail { get; set; }
        public string SentFromUserName { get; set; }
        public string EmailBodyTemplate { get; set; }
        public string EmailSubjectTemplate { get; set; }
        public bool AttachExcelOfBreaches { get; set; }
        public ICollection<EscalationsFrameworkBreachSource> BreachSources { get; set; }

        #endregion

        #region [ Not Mapped EF Properties ]

        [NotMapped]
        public string InternalSentFromUserName
        {
            get
            {
                return string.IsNullOrEmpty(this.SentFromUserName) ? ConfigurationManager.AppSettings["SystemSentFromName"] : this.SentFromUserName;
            }
        }
        [NotMapped]
        public string InternalSentFromEmail
        {
            get
            {
                return string.IsNullOrEmpty(this.SentFromEmail) ? ConfigurationManager.AppSettings["SystemSentFromEmailAddress"] : this.SentFromEmail;
            }
        }

        #endregion

        #region [ Methods ]

        internal List<BreachLog> GetUniqueBreachLogsFromBreachSources(VirtualTrainerContext ctx)
        {
            LoadRequiredContextObjects(ctx);

            List<BreachLog> breachLogs = new List<BreachLog>();

            foreach (EscalationsFrameworkBreachSource source in this.BreachSources.ToList())
            {
                breachLogs.AddRange(source.GetBreaches(ctx));
            }
            return GetUniqueBreachesFromList(breachLogs);
        }
        internal List<BreachLog> GetUniqueBreachesFromList(List<BreachLog> breachLogs)
        {
            return breachLogs.GroupBy(u => u.ContextRef).Select(group => group.First()).ToList();
        }
        internal List<User> GetUniqueUsersFromBreachSources(VirtualTrainerContext ctx)
        {
            LoadRequiredContextObjects(ctx);

            List<User> breachSourceUsers = new List<User>();

            foreach (EscalationsFrameworkBreachSource source in this.BreachSources)
            {
                breachSourceUsers.AddRange(source.GetUsers(ctx));
            }
            return GetUniqueUsersFromList(breachSourceUsers);
        }
        internal List<User> GetUniqueUsersFromList(List<User> users)
        {
            return users.GroupBy(u => u.Id).Select(group => group.First()).ToList();
        }

        #endregion

        #region [ Internal Methods ]

        internal void SendEmail(string body, string toEmail, string subject, EscalationsFrameworkConfigEmailRazorModel razorModel, VirtualTrainerContext ctx, SmtpClient SmtpClient)
        {
            string pathToExcelDoc = string.Empty;
            try
            {
               // using (SmtpClient SmtpServer = new SmtpClient())
                using (MailMessage mail = new MailMessage())
                {
                    mail.Subject = subject;
                    mail.IsBodyHtml = true;
                    mail.Body = body;
                    mail.To.Add(toEmail);

                    if (razorModel.AttachExcelofBreaches)
                    {
                        pathToExcelDoc = GenerateExcelDocOFBreaches(razorModel, ctx);
                        mail.Attachments.Add(new Attachment(pathToExcelDoc));
                    }

                    SmtpClient.Send(mail);
                }
            }
            catch (Exception ex)
            {
                SystemLog log = new SystemLog(ex, this.Project);
                ctx.SystemLogs.Add(log);
                ctx.SaveChanges();
            }
            finally
            {
                if (!string.IsNullOrEmpty(pathToExcelDoc))
                {
                    File.Delete(pathToExcelDoc);
                }
            }
        }

        internal string GenerateExcelDocOFBreaches(EscalationsFrameworkConfigEmailRazorModel razorModel, VirtualTrainerContext ctx)
        {
            ExcelUtlity eu = new ExcelUtlity();
            //List<BreachLogExcelReport> logsForExcel = razorModel.BreachLogs.Select(a => a.getBreachDetailsForExcel(ctx)).ToList();

            List<BreachLogExcelReport> logsForExcel = razorModel.BreachLogs.Select(b => new BreachLogExcelReport()
            {
                BreachCount = b.GetBreachCountForContextRef(ctx),
                BreachDisplayHtml = b.BreachDisplayText,
                BreachField1 = b.RuleBreachFieldOne,
                BreachField2 = b.RuleBreachFieldTwo,
                BreachField3 = b.RuleBreachFieldThree,
                BreachField4 = b.RuleBreachFieldFour,
                BreachField5 = b.RuleBreachFieldFive,
                BreachField6 = b.RuleBreachFieldSix,
                BreachField7 = b.RuleBreachFieldSeven,
                ContextRef = b.ContextRef,
                TimeStamp = b.TimeStamp,
                ProjectId = b.ProjectId.ToString(),
                RuleConfigName = b.RuleConfigurationName,
                RuleConfigStoredProc = b.StoredProecdureName,
                UserName = b.UserName,
                RuleName = b.RuleName
            }).ToList();


            List<objectProperty> objectProperties = ExcelUtlity.GetOrderedObjectPropertyDetails<BreachLogExcelReport>();
            return eu.WriteObjectsToExcel<BreachLogExcelReport>(logsForExcel, objectProperties);
        }

        internal new void LoadRequiredContextObjects(VirtualTrainerContext ctx)
        {
            if (!ctx.Entry(this).Collection("BreachSources").IsLoaded)
            {
                ctx.Entry(this).Collection("BreachSources").Load();
            }
            base.LoadRequiredContextObjects(ctx);
        }

        #endregion
    }
}
