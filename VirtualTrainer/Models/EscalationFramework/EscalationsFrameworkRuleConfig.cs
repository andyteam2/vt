﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualTrainer.Utilities;

namespace VirtualTrainer
{
    public abstract class EscalationsFrameworkRuleConfig
    {
        #region [ EF Properties ]

        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("Project")]
        [Required]
        public Guid ProjectId { get; set; }
        public Project Project { get; set; }
        [ForeignKey("EscalationsFramework")]
        [Required]
        public Guid EscalationsFrameworkId { get; set; }
        public EscalationsFramework EscalationsFramework { get; set; }
        public int BreachCount { get; set; }
        [ForeignKey("Schedule")]
        [Required]
        public int ScheduleId { get; set; }
        public Schedule Schedule { get; set; }
        //[Required]
        //public DateTime ScheduleStartDate { get; set; }
        public DateTime? LastRunDate { get; set; }
        //[ForeignKey("ScheduleFrequency")]
        //public int ScheduleFrequencyId { get; set; }
        //[Required]
        //public ScheduleFrequency ScheduleFrequency { get; set; }
        // public ICollection<EscalationsActionTakenLog> EscalationsFrameworkActionsTaken { get; set; }
        public ICollection<ActionTakenLog> ActionTakenLog { get; set; }

        #endregion

        #region [ Non EF Properties ]

        [NotMapped]
        public string ScheduleName { get; set; }

        #endregion

        #region [ Abstract Methods ]

        public abstract void RunEscalationConfiguration(VirtualTrainerContext ctx, bool updateTimeStamp, string ServerRazorEmailTemplatePath);

        #endregion

        #region [ internal Methods ]

        //internal bool IsScheduledToRun(VirtualTrainerContext ctx)
        //{
        //    // Don't allow to run if the scheduled start date is not today or in the past.
        //    if (this.ScheduleStartDate <= DateTime.Now)
        //    {
        //        switch (this.ScheduleFrequencyId)
        //        {
        //            case (int)EscalationsFrameworkScheduleFrequencyEnum.Development:
        //                return true;
        //            case (int)EscalationsFrameworkScheduleFrequencyEnum.DailyAllDays:
        //                // if last run is not today then ok to run.
        //                return (this.LastRunDate == null || this.LastRunDate > DateTime.Now.Date);
        //            case (int)EscalationsFrameworkScheduleFrequencyEnum.DailyWeekdays:
        //                // if today is a week day and hasnt been run today.
        //                return
        //                    (
        //                        DateTime.Now.Date.IsWeekday() &&
        //                        (this.LastRunDate == null || this.LastRunDate > DateTime.Now.Date)
        //                    );
        //            case (int)EscalationsFrameworkScheduleFrequencyEnum.DailyWeekends:
        //                // If Today is a weekend and it hasnt run today yet.
        //                return
        //                    (
        //                        !DateTime.Now.Date.IsWeekday() &&
        //                        (this.LastRunDate == null || this.LastRunDate > DateTime.Now.Date)
        //                    );
        //            case (int)EscalationsFrameworkScheduleFrequencyEnum.WeeklyMon:
        //                // If today is Monday and it hasnt been run since at least last Monday
        //                return
        //                    (
        //                        DateTime.Now.DayOfWeek == DayOfWeek.Monday &&
        //                        (this.LastRunDate == null || this.LastRunDate <= DateTime.Now.AddDays(-7))
        //                    );
        //            case (int)EscalationsFrameworkScheduleFrequencyEnum.WeeklyTue:
        //                return
        //                    (
        //                        DateTime.Now.DayOfWeek == DayOfWeek.Tuesday &&
        //                        (this.LastRunDate == null || this.LastRunDate <= DateTime.Now.AddDays(-7))
        //                    );
        //            case (int)EscalationsFrameworkScheduleFrequencyEnum.WeeklyWed:
        //                return
        //                    (
        //                        DateTime.Now.DayOfWeek == DayOfWeek.Wednesday &&
        //                        (this.LastRunDate == null || this.LastRunDate <= DateTime.Now.AddDays(-7))
        //                    );
        //            case (int)EscalationsFrameworkScheduleFrequencyEnum.WeeklyThur:
        //                return
        //                    (
        //                        DateTime.Now.DayOfWeek == DayOfWeek.Thursday &&
        //                        (this.LastRunDate == null || this.LastRunDate <= DateTime.Now.AddDays(-7))
        //                    );
        //            case (int)EscalationsFrameworkScheduleFrequencyEnum.WeeklyFri:
        //                return
        //                    (
        //                        DateTime.Now.DayOfWeek == DayOfWeek.Friday &&
        //                        (this.LastRunDate == null || this.LastRunDate <= DateTime.Now.AddDays(-7))
        //                    );
        //            case (int)EscalationsFrameworkScheduleFrequencyEnum.WeeklySat:
        //                return
        //                    (
        //                        DateTime.Now.DayOfWeek == DayOfWeek.Saturday &&
        //                        (this.LastRunDate == null || this.LastRunDate <= DateTime.Now.AddDays(-7))
        //                    );
        //            case (int)EscalationsFrameworkScheduleFrequencyEnum.WeeklySun:
        //                return
        //                    (
        //                        DateTime.Now.DayOfWeek == DayOfWeek.Sunday &&
        //                        (this.LastRunDate == null || this.LastRunDate <= DateTime.Now.AddDays(-7))
        //                    );
        //            case (int)EscalationsFrameworkScheduleFrequencyEnum.MonthlyFirstDay:
        //                return
        //                    (
        //                        DateTime.Now.IsFirstOfMonth() &&
        //                        (this.LastRunDate == null || this.LastRunDate <= DateTime.Now.AddMonths(-1))
        //                    );
        //            case (int)EscalationsFrameworkScheduleFrequencyEnum.MonthlyLastDay:
        //                return
        //                    (
        //                        DateTime.Now.IsLastOfMonth() &&
        //                        (this.LastRunDate == null || this.LastRunDate <= DateTime.Now.AddMonths(-1))
        //                    );
        //        }
        //    }
        //    return false;
        //}
        internal List<BreachLog> ReturnOnlyUniqueBreaches(List<BreachLog> breaches)
        {
            return breaches.GroupBy(u => u.ContextRef).Select(group => group.First()).ToList();
        }
        public void LoadRequiredContextObjects(VirtualTrainerContext ctx)
        {
            if (!ctx.Entry(this).Reference("Project").IsLoaded)
            {
                ctx.Entry(this).Reference("Project").Load();
            }
            if (!ctx.Entry(this).Reference("EscalationsFramework").IsLoaded)
            {
                ctx.Entry(this).Reference("EscalationsFramework").Load();
            }
            if (!ctx.Entry(this).Collection("ActionTakenLog").IsLoaded)
            {
                ctx.Entry(this).Collection("ActionTakenLog").Load();
            }
            if (!ctx.Entry(this).Reference("Schedule").IsLoaded)
            {
                ctx.Entry(this).Reference("Schedule").Load();
            }
        }

        #endregion
    }
}
