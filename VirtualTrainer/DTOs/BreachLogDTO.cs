﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualTrainer
{
    public class BreachLogDTO
    {
        public int Id { get; set; }
        public string RuleName { get; set; }
        public string RuleConfigurationName { get; set; }
        public string UserName { get; set; }
        public string OfficeName { get; set; }
        public string TeamName { get; set; }
        public bool IsArchived { get; set; }
        public DateTime TimeStamp { get; set; }
        public string ContextRef { get; set; }
        public string BreachDisplayHTML { get; set; }
    }
}
