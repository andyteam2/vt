﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VirtualTrainer;
using System.Linq;
using System.Data.Entity;

namespace VTTests
{
    /// <summary>
    /// Summary description for UnitTestUser
    /// </summary>
    [TestClass]
    public class UnitTest_Breaches
    {
        private string TestProjectId = "A896DA6D-F233-42DE-8CF6-A3D21FF42C6D";
        #region [ Default class bumf ]

        public UnitTest_Breaches() { }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        #endregion

        [TestMethod]
        public void Test_UserBreach()
        {
            using (var ctx = new VirtualTrainerContext())
            {
                User user = ctx.User.Where(o => o.Id == 91).Include("BreachLogs").FirstOrDefault();
                List<BreachLog> returnBreaches = new List<BreachLog>();
                List<BreachLog> logs = user.BreachLogs.OrderBy(a => a.TimeStamp).ToList();
                List<BreachLog> logsSingle = user.BreachLogs.OrderBy(a => a.TimeStamp).GroupBy(b => b.ContextRef).Select(c=> {
                    c.LastOrDefault().BreachLiveContextRefCount = c.Count();
                    c.LastOrDefault().FirstBreachDate = c.FirstOrDefault().TimeStamp;
                    c.LastOrDefault().User = null;
                    return c.LastOrDefault();
                }).ToList();


                //foreach (var log in logs)
                //{
                //    string key = log.Key;
                //    int count = log.Count();
                //    DateTime firstDate = log.FirstOrDefault().TimeStamp;
                //}
                //int logCount = office.BreachLogs.Count();
            }
        }
    }
}
